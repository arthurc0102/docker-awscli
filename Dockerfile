FROM python:3.7-alpine

RUN pip install --upgrade --no-cache-dir pip awsebcli awscli
